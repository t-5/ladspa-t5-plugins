#!/bin/bash

cd ../src
srcdir=`pwd`

for arch in amd64 i386 armhf; do
    cd $srcdir
    rm -rdf build_${arch}
    case $arch in
        amd64)
            crossfile=""
            ;;
        i386)
            crossfile="--cross-file=cross_i386.txt"
            ;;
        armhf)
            crossfile="--cross-file=cross_armhf.txt"
            ;;
        arm64)
            crossfile="--cross-file=cross_arm64.txt"
            ;;
    esac
    meson build_${arch} $crossfile
    cd build_${arch}
    LC_ALL=c ninja
done
