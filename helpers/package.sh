#!/bin/bash

cd ..
srcdir=`pwd`

VERSION=`cat .previous_version`
echo -n "Version (previous version was $VERSION: "
read VERSION

rm -f builds/*.deb

for arch in amd64 i386 armhf arm64; do
    cd $srcdir
    sudo rm -rdf debian
    mkdir -p debian/DEBIAN
    mkdir -p debian/usr/lib/ladspa
    cat control.in | sed "s#_VERSION_#${VERSION}#" | sed "s#_ARCH_#${arch}#" > debian/DEBIAN/control
    cp src/build_${arch}/*.so debian/usr/lib/ladspa
    chmod 644 debian/usr/lib/ladspa/*.so
    sudo chown -R root.root debian/
    dpkg --build debian || exit 1
    mv debian.deb builds/ladspa-t5-plugins_${VERSION}_${arch}.deb || exit 1
done

sudo rm -rdf debian
aptly repo add t-5 builds && echo -n "$VERSION" > .previous_version
