/* t5_subsample_delay.c

   Free software by Juergen Herrmann, t-5@t-5.eu. Do with it, whatever you
   want. No warranty. None, whatsoever. Also see license.txt .

   This LADSPA plugin provides a subsample accurate delay with up to 256k
   samples delay (maximum actual wallclock time delay available depends on
   the sample rate used)

   This file has poor memory protection. Failures during malloc() will
   not recover nicely. */

/*****************************************************************************/

#include <math.h>
#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <string.h>
#include <sys/mman.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <time.h>
#include <ladspa.h>
#include "helpers.h"


#define PORT_INPUT       0
#define PORT_OUTPUT      1
#define PORT_GAIN        2
#define PORT_DELAY       3
#define PORT_MMAPFNAME   4
#define PORTCOUNT        5
#define RING_BUFFER_LENGTH 262144
#define MIN(a,b) (((a)<(b))?(a):(b))


static void plugin_init(void) __attribute__((constructor)); 
static void plugin_deinit(void) __attribute__((destructor)); 


/* instance data for the SubsampleDelay filter */
typedef struct {
    long created_ns;
    time_t created_s;
    LADSPA_Data sampleRate;
    LADSPA_Data* mmapArea;
    LADSPA_Data* mmapFname;
    LADSPA_Data* gain;
    // last previously processed input sample
    LADSPA_Data x1;
    // delay and previous delay
    LADSPA_Data* delay;
    LADSPA_Data previousDelay;
    // ring buffer for samples
    LADSPA_Data* ringBuffer;
    int ringBufferPos;
    // port pointers
    LADSPA_Data* input;
    LADSPA_Data* output;
} SubsampleDelay;


/* setup shared memory area to enable parametrization at runtime from external processes */
void setupMmapFileForSubsampleDelay(SubsampleDelay* instance) {
    TimeMmapStruct ret;
    ret = setupMmapFile("SubsampleDelay", *(instance->mmapFname), PORTCOUNT); 
    instance->mmapArea = ret.mmap;
    instance->created_s = ret.s;
    instance->created_ns = ret.ns;
}


/* Construct a new plugin instance. */
LADSPA_Handle instantiateSubsampleDelay(const LADSPA_Descriptor* descriptor,
                                     unsigned long sampleRate) {
    SubsampleDelay* instance;
    instance = (SubsampleDelay*)malloc(sizeof(SubsampleDelay));
    if (instance) {
        instance->sampleRate = (LADSPA_Data)sampleRate;
        instance->mmapArea = NULL;
        instance->ringBuffer = malloc(sizeof(LADSPA_Data) * RING_BUFFER_LENGTH);
    }  
    return instance;
}


/* Initialise and activate a plugin instance. */
void activateSubsampleDelay(LADSPA_Handle ladspa_handle) {
    SubsampleDelay* instance;
    instance = (SubsampleDelay*)ladspa_handle;
    *(instance->gain) = 0;
    *(instance->delay) = 0;
    instance->previousDelay = 0;
    instance->x1 = 0;
    instance->ringBufferPos = 0;
    for (int i=0; i < RING_BUFFER_LENGTH; i++) {
        *(instance->ringBuffer + i) = 0;
    }
}


/* Connect a port to a data location.  */
void connectPortToSubsampleDelay(LADSPA_Handle ladspa_handle,
                              unsigned long port,
                              LADSPA_Data* dataLocation) {  
    SubsampleDelay* instance;
    instance = (SubsampleDelay*)ladspa_handle;
    switch (port) {
    case PORT_INPUT:
        instance->input = dataLocation;
        break;
    case PORT_OUTPUT:
        instance->output = dataLocation;
        break;
    case PORT_GAIN:
        instance->gain = dataLocation;
        break;
    case PORT_DELAY:
        instance->delay = dataLocation;
        break;
    case PORT_MMAPFNAME:
        instance->mmapFname = dataLocation;
        break;
    }
}


/* Run the filter algorithm for a block of SampleCount samples. */
void runSubsampleDelay(LADSPA_Handle ladspa_handle,
                       unsigned long SampleCount) {

    LADSPA_Data* input;
    LADSPA_Data* output;
    SubsampleDelay* instance;
    int delaySamples;
    float delaySubSamples;
    int myRingBufferPos;
    unsigned long sampleIndex;
    LADSPA_Data unchanged = 0.0;
    LADSPA_Data changed;
    LADSPA_Data gainFactor;
    LADSPA_Data* mmptr;
    float x0, x1; // holds previously processed input sample

    instance = (SubsampleDelay*)ladspa_handle;
    input = instance->input;
    output = instance->output;
    // memcpy parameters over from mmapped area
    mmptr = instance->mmapArea;
    if (mmptr != NULL) {
        memcpy(&changed, mmptr, sizeof(LADSPA_Data));
        if (changed != 0.0) {
            mmptr += 1;
            memcpy(instance->delay, mmptr, sizeof(LADSPA_Data));
            mmptr += 1;
            memcpy(instance->gain, mmptr, sizeof(LADSPA_Data));
        }
        // reset changed flag to re-enable parametrization by control inputs
        memcpy(instance->mmapArea, &unchanged, sizeof(LADSPA_Data));
    } else if (*(instance->mmapFname) != 0.0) {
        setupMmapFileForSubsampleDelay(instance);
    }
    gainFactor = dbToGainFactor(*(instance->gain));
    if (*(instance->delay) != 0) {
        // get previosuly processed sample
        x1 = instance->x1;
        // calculate delaySamples, bounded above by RING_BUFFER_LENGTH, floored by cast to int
        delaySamples = MIN(RING_BUFFER_LENGTH, (int)(*(instance->delay) / 1000 * instance->sampleRate));
        // calculate delaySubSamples
        delaySubSamples = (*(instance->delay) / 1000 * instance->sampleRate) - 
                    (int)((*(instance->delay) / 1000 * instance->sampleRate));
        // zero ringBuffer and ringBufferPos if delay has changed
        if (*(instance->delay) != instance->previousDelay) {
            for (int i=0; i < RING_BUFFER_LENGTH; i++) {
                *(instance->ringBuffer + i) = 0;
            }
            instance->ringBufferPos = 0;
            instance->previousDelay = *(instance->delay);
            x1 = 0;
        }
        myRingBufferPos = (instance->ringBufferPos - delaySamples + RING_BUFFER_LENGTH) % RING_BUFFER_LENGTH;
        for (sampleIndex = 0; sampleIndex < SampleCount; sampleIndex++) {
            *(instance->ringBuffer + instance->ringBufferPos++) = *(input++);
            x0 = *(instance->ringBuffer + myRingBufferPos++);
            *(output++) = ((1 - delaySubSamples) * x0 + delaySubSamples * x1) * gainFactor;
            x1 = x0;
            if (myRingBufferPos == RING_BUFFER_LENGTH) {
                myRingBufferPos = 0;
            }
            if (instance->ringBufferPos == RING_BUFFER_LENGTH) {
                instance->ringBufferPos = 0;
            }
        }
    } else {
        for (sampleIndex = 0; sampleIndex < SampleCount; sampleIndex++) {
            *(output++) = *(input++) * gainFactor;
        }
        x1 = *(output - 1);
    }
    // store previously calculated sample for later
    instance->x1 = x1;
}


/* Throw away a SubsampleDelay instance. */
void cleanupSubsampleDelay(LADSPA_Handle ladspa_handle) {
    SubsampleDelay * instance;
    instance = (SubsampleDelay*)ladspa_handle;
    if (instance->mmapArea != NULL) {
        cleanupMmapFile("SubsampleDelay",
                        *(instance->mmapFname),
                        instance->created_s,
                        instance->created_ns);
        free(instance);
    }
}


LADSPA_Descriptor * subsampleDelayInstanceDescriptor = NULL;


/* called automatically when the plugin library is first loaded. */
void plugin_init() {

    char ** pcportNames;
    LADSPA_PortDescriptor * portDescriptors;
    LADSPA_PortRangeHint * portRangeHints;
    
    subsampleDelayInstanceDescriptor
      = (LADSPA_Descriptor *)malloc(sizeof(LADSPA_Descriptor));

    if (subsampleDelayInstanceDescriptor != NULL) {
    
        subsampleDelayInstanceDescriptor->UniqueID
            = 5549;
        subsampleDelayInstanceDescriptor->Label
            = strdup("subsample_delay");
        subsampleDelayInstanceDescriptor->Properties
            = LADSPA_PROPERTY_HARD_RT_CAPABLE;
        subsampleDelayInstanceDescriptor->Name 
            = strdup("T5's SubSample Accurate Delay");
        subsampleDelayInstanceDescriptor->Maker
            = strdup("Juergen Herrmann (t-5@t-5.eu)");
        subsampleDelayInstanceDescriptor->Copyright
            = strdup("3-clause BSD licence");
        subsampleDelayInstanceDescriptor->PortCount
            = PORTCOUNT;
        portDescriptors
            = (LADSPA_PortDescriptor*)calloc(PORTCOUNT, sizeof(LADSPA_PortDescriptor));
        subsampleDelayInstanceDescriptor->PortDescriptors
            = (const LADSPA_PortDescriptor*)portDescriptors;
        portDescriptors[PORT_INPUT]
            = LADSPA_PORT_INPUT | LADSPA_PORT_AUDIO;
        portDescriptors[PORT_OUTPUT]
            = LADSPA_PORT_OUTPUT | LADSPA_PORT_AUDIO;
        portDescriptors[PORT_GAIN]
            = LADSPA_PORT_INPUT | LADSPA_PORT_CONTROL;
        portDescriptors[PORT_DELAY]
            = LADSPA_PORT_INPUT | LADSPA_PORT_CONTROL;
        portDescriptors[PORT_MMAPFNAME]
            = LADSPA_PORT_INPUT | LADSPA_PORT_CONTROL;
        pcportNames
            = (char **)calloc(PORTCOUNT, sizeof(char *));
        subsampleDelayInstanceDescriptor->PortNames 
            = (const char **)pcportNames;
        pcportNames[PORT_INPUT]
            = strdup("Input");
        pcportNames[PORT_OUTPUT]
            = strdup("Output");
        pcportNames[PORT_GAIN]
            = strdup("Gain [dB]");
        pcportNames[PORT_DELAY]
            = strdup("Delay [ms]");
        pcportNames[PORT_MMAPFNAME]
            = strdup("MMAP-Filename-Part");
        portRangeHints = ((LADSPA_PortRangeHint*)
            calloc(PORTCOUNT, sizeof(LADSPA_PortRangeHint)));
        subsampleDelayInstanceDescriptor->PortRangeHints
            = (const LADSPA_PortRangeHint*)portRangeHints;
        // Gain ------------------------------------------------------------ */
        portRangeHints[PORT_GAIN].HintDescriptor
            = (LADSPA_HINT_BOUNDED_BELOW 
            | LADSPA_HINT_BOUNDED_ABOVE
            | LADSPA_HINT_DEFAULT_0);
        portRangeHints[PORT_GAIN].LowerBound 
            = -12;
        portRangeHints[PORT_GAIN].UpperBound
            = 12;
        // Delay ----------------------------------------------------------- */
        portRangeHints[PORT_DELAY].HintDescriptor
            = (LADSPA_HINT_BOUNDED_BELOW 
            | LADSPA_HINT_BOUNDED_ABOVE
            | LADSPA_HINT_DEFAULT_0);
        portRangeHints[PORT_DELAY].LowerBound 
            = 0;
        portRangeHints[PORT_DELAY].UpperBound
            // ring buffer size on assumed max sample rate times 1000 because value is in milliseconds
            = 1000;
        // MMAP Filename --------------------------------------------------- */
        portRangeHints[PORT_MMAPFNAME].HintDescriptor
            = (LADSPA_HINT_BOUNDED_BELOW 
            | LADSPA_HINT_BOUNDED_ABOVE
            | LADSPA_HINT_DEFAULT_0);
        portRangeHints[PORT_MMAPFNAME].LowerBound 
            = 0;
        portRangeHints[PORT_MMAPFNAME].UpperBound
            = 10000000000;
        // In- and Outpus -------------------------------------------------- */
        portRangeHints[PORT_INPUT].HintDescriptor
            = 0;
        portRangeHints[PORT_OUTPUT].HintDescriptor
            = 0;
        subsampleDelayInstanceDescriptor->instantiate 
            = instantiateSubsampleDelay;
        subsampleDelayInstanceDescriptor->connect_port 
            = connectPortToSubsampleDelay;
        subsampleDelayInstanceDescriptor->activate
            = activateSubsampleDelay;
        subsampleDelayInstanceDescriptor->run
            = runSubsampleDelay;
        subsampleDelayInstanceDescriptor->run_adding
            = NULL;
        subsampleDelayInstanceDescriptor->set_run_adding_gain
            = NULL;
        subsampleDelayInstanceDescriptor->deactivate
            = NULL;
        subsampleDelayInstanceDescriptor->cleanup
            = cleanupSubsampleDelay;
    }
}
  

/* called automatically when the library is unloaded. */
void plugin_deinit() {
    deleteDescriptor(subsampleDelayInstanceDescriptor);
}


/* Return a descriptor of the requested plugin types. */
const LADSPA_Descriptor * ladspa_descriptor(unsigned long index) {
    /* Return the requested descriptor or null if the index is out of range. */
    switch (index) {
    case 0:
        return subsampleDelayInstanceDescriptor;
    default:
        return NULL;
    }
}
