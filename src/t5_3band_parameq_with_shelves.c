/* t5_3band_parameq_with_shelves.c

   Free software by Juergen Herrmann, t-5@t-5.eu. Do with it, whatever you
   want. No warranty. None, whatsoever. Also see license.txt .

   This LADSPA plugin provides a three band parametric equalizer with
   shelving low- and highpass filters based on biquad coefficients

   This file has poor memory protection. Failures during malloc() will
   not recover nicely. */

/*****************************************************************************/

#include <math.h>
#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <string.h>
#include <sys/mman.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <time.h>
#include <ladspa.h>
#include "runbiquad.h"
#include "helpers.h"


#define SF_INPUT       0
#define SF_OUTPUT      1
#define SF_LOW_F       2
#define SF_LOW_G       3
#define SF_LOW_Q       4
#define SF_P1_F        5
#define SF_P1_G        6
#define SF_P1_Q        7
#define SF_P2_F        8
#define SF_P2_G        9
#define SF_P2_Q       10
#define SF_P3_F       11
#define SF_P3_G       12
#define SF_P3_Q       13
#define SF_HIGH_F     14
#define SF_HIGH_G     15
#define SF_HIGH_Q     16
#define SF_GAIN       17
#define SF_MMAPFNAME  18
#define PORTCOUNT     19


static void plugin_init(void) __attribute__((constructor)); 
static void plugin_deinit(void) __attribute__((destructor)); 


/* Instance data for the ThreeBandParametricEqWithShelves filter */
typedef struct {
    long created_ns;
    time_t created_s;
    LADSPA_Data sampleRate;
    LADSPA_Data* mmapArea;
    // previous sample storage
    RunBiquadPrevSamples* prevSamples_LOW;
    RunBiquadPrevSamples* prevSamples_P1;
    RunBiquadPrevSamples* prevSamples_P2;
    RunBiquadPrevSamples* prevSamples_P3;
    RunBiquadPrevSamples* prevSamples_HIGH;
    // port pointers
    LADSPA_Data* input;
    LADSPA_Data* output;
    LADSPA_Data* lowF;
    LADSPA_Data* lowG;
    LADSPA_Data* lowQ;
    LADSPA_Data* p1F;
    LADSPA_Data* p1G;
    LADSPA_Data* p1Q;
    LADSPA_Data* p2F;
    LADSPA_Data* p2G;
    LADSPA_Data* p2Q;
    LADSPA_Data* p3F;
    LADSPA_Data* p3G;
    LADSPA_Data* p3Q;
    LADSPA_Data* highF;
    LADSPA_Data* highG;
    LADSPA_Data* highQ;
    LADSPA_Data* gain;
    LADSPA_Data* mmapFname;
} ThreeBandParametricEqWithShelves;


/* setup shared memory area to enable parametrization at runtime from external processes */
void setupMmapFileForThreeBandParametricEqWithShelves(ThreeBandParametricEqWithShelves* instance) {
    TimeMmapStruct ret;
    ret = setupMmapFile("3BandParamEqWithShelves", *(instance->mmapFname), PORTCOUNT); 
    instance->mmapArea = ret.mmap;
    instance->created_s = ret.s;
    instance->created_ns = ret.ns;
}


/* Construct a new plugin instance. */
LADSPA_Handle instantiateThreeBandParametricEqWithShelves(const LADSPA_Descriptor* ladspaDescriptor,
                                                          unsigned long sampleRate) {
    ThreeBandParametricEqWithShelves* instance;
    instance = (ThreeBandParametricEqWithShelves*)malloc(sizeof(ThreeBandParametricEqWithShelves));
    if (instance) {
        instance->sampleRate = (LADSPA_Data)sampleRate;
        instance->mmapArea = NULL;
        instance->prevSamples_LOW = malloc(sizeof(RunBiquadPrevSamples));
        instance->prevSamples_P1 = malloc(sizeof(RunBiquadPrevSamples));
        instance->prevSamples_P2 = malloc(sizeof(RunBiquadPrevSamples));
        instance->prevSamples_P3 = malloc(sizeof(RunBiquadPrevSamples));
        instance->prevSamples_HIGH = malloc(sizeof(RunBiquadPrevSamples));
    }  
    return instance;
}


/* Initialise and activate a plugin instance. */
void activateThreeBandParametricEqWithShelves(LADSPA_Handle Instance) {
    ThreeBandParametricEqWithShelves * instance;
    instance = (ThreeBandParametricEqWithShelves *)Instance;
    instance->prevSamples_LOW->xprev1 = 0;
    instance->prevSamples_LOW->yprev1 = 0;
    instance->prevSamples_LOW->xprev2 = 0;
    instance->prevSamples_LOW->yprev2 = 0;
    instance->prevSamples_P1->xprev1 = 0;
    instance->prevSamples_P1->yprev1 = 0;
    instance->prevSamples_P1->xprev2 = 0;
    instance->prevSamples_P1->yprev2 = 0;
    instance->prevSamples_P2->xprev1 = 0;
    instance->prevSamples_P2->yprev1 = 0;
    instance->prevSamples_P2->xprev2 = 0;
    instance->prevSamples_P2->yprev2 = 0;
    instance->prevSamples_P3->xprev1 = 0;
    instance->prevSamples_P3->yprev1 = 0;
    instance->prevSamples_P3->xprev2 = 0;
    instance->prevSamples_P3->yprev2 = 0;
    instance->prevSamples_HIGH->xprev1 = 0;
    instance->prevSamples_HIGH->yprev1 = 0;
    instance->prevSamples_HIGH->xprev2 = 0;
    instance->prevSamples_HIGH->yprev2 = 0;
}


/* Connect a port to a data location.  */
void connectPortToThreeBandParametricEqWithShelves(LADSPA_Handle Instance,
                                                   unsigned long Port,
                                                   LADSPA_Data * DataLocation) {  
    ThreeBandParametricEqWithShelves * instance;
    instance = (ThreeBandParametricEqWithShelves *)Instance;
    switch (Port) {
    case SF_INPUT:
        instance->input = DataLocation;
        break;
    case SF_OUTPUT:
        instance->output = DataLocation;
        break;
    case SF_LOW_F:
        instance->lowF = DataLocation;
        break;
    case SF_LOW_G:
        instance->lowG = DataLocation;
        break;
    case SF_LOW_Q:
        instance->lowQ = DataLocation;
        break;
    case SF_P1_F:
        instance->p1F = DataLocation;
        break;
    case SF_P1_G:
        instance->p1G = DataLocation;
        break;
    case SF_P1_Q:
        instance->p1Q = DataLocation;
        break;
    case SF_P2_F:
        instance->p2F = DataLocation;
        break;
    case SF_P2_G:
        instance->p2G = DataLocation;
        break;
    case SF_P2_Q:
        instance->p2Q = DataLocation;
        break;
    case SF_P3_F:
        instance->p3F = DataLocation;
        break;
    case SF_P3_G:
        instance->p3G = DataLocation;
        break;
    case SF_P3_Q:
        instance->p3Q = DataLocation;
        break;
    case SF_HIGH_F:
        instance->highF = DataLocation;
        break;
    case SF_HIGH_G:
        instance->highG = DataLocation;
        break;
    case SF_HIGH_Q:
        instance->highQ = DataLocation;
        break;
    case SF_GAIN:
        instance->gain = DataLocation;
        break;
    case SF_MMAPFNAME:
        instance->mmapFname = DataLocation;
        break;
    }
}


/* Run the filter algorithm for a block of sampleCount samples. */
void runThreeBandParametricEqWithShelves(LADSPA_Handle ladspaHandle,
                                         unsigned long sampleCount) {

    LADSPA_Data * input;
    LADSPA_Data * output;
    ThreeBandParametricEqWithShelves* instance;
    BiquadCoeffs coeffs_LOW, coeffs_P1, coeffs_P2, coeffs_P3, coeffs_HIGH;
    unsigned long sampleIndex;
    LADSPA_Data unchanged = 0.0;
    LADSPA_Data changed;
    LADSPA_Data* mmptr;
    float gainFactor;

    instance = (ThreeBandParametricEqWithShelves*)ladspaHandle;
    input = instance->input;
    output = instance->output;

    // memcpy parameters over from mmapped area
    mmptr = instance->mmapArea;
    if (mmptr != NULL) {
        memcpy(&changed, mmptr, sizeof(LADSPA_Data));
        if (changed != 0.0) {
            mmptr += 1;
            memcpy(instance->lowF, mmptr, sizeof(LADSPA_Data));
            mmptr += 1;
            memcpy(instance->lowG, mmptr, sizeof(LADSPA_Data));
            mmptr += 1;
            memcpy(instance->lowQ, mmptr, sizeof(LADSPA_Data));
            mmptr += 1;
            memcpy(instance->p1F, mmptr, sizeof(LADSPA_Data));
            mmptr += 1;
            memcpy(instance->p1G, mmptr, sizeof(LADSPA_Data));
            mmptr += 1;
            memcpy(instance->p1Q, mmptr, sizeof(LADSPA_Data));
            mmptr += 1;
            memcpy(instance->p2F, mmptr, sizeof(LADSPA_Data));
            mmptr += 1;
            memcpy(instance->p2G, mmptr, sizeof(LADSPA_Data));
            mmptr += 1;
            memcpy(instance->p2Q, mmptr, sizeof(LADSPA_Data));
            mmptr += 1;
            memcpy(instance->p3F, mmptr, sizeof(LADSPA_Data));
            mmptr += 1;
            memcpy(instance->p3G, mmptr, sizeof(LADSPA_Data));
            mmptr += 1;
            memcpy(instance->p3Q, mmptr, sizeof(LADSPA_Data));
            mmptr += 1;
            memcpy(instance->highF, mmptr, sizeof(LADSPA_Data));
            mmptr += 1;
            memcpy(instance->highG, mmptr, sizeof(LADSPA_Data));
            mmptr += 1;
            memcpy(instance->highQ, mmptr, sizeof(LADSPA_Data));
            mmptr += 1;
            memcpy(instance->gain, mmptr, sizeof(LADSPA_Data));
        }
        // reset changed flag to re-enable parametrization by control inputs
        memcpy(instance->mmapArea, &unchanged, sizeof(LADSPA_Data));
    } else if (*(instance->mmapFname) != 0.0) {
        setupMmapFileForThreeBandParametricEqWithShelves(instance);
    }
    // apply gain if gainFactor != 1, in any case make sure output buf is filled
    // as it will be used by the run2ndOrderBiquad() functions.
    if (*(instance->gain) == 0) {
        for (sampleIndex = 0; sampleIndex < sampleCount; sampleIndex++) {
            *(output++) = *(input++);
        }
    } else {
        gainFactor = dbToGainFactor(*(instance->gain));
        for (sampleIndex = 0; sampleIndex < sampleCount; sampleIndex++) {
            *(output++) = *(input++) * gainFactor;
        }
    }
    // FILTER PROCESSING FOR LOW SHELF EQ ///////////////////////////////////////
    if (*(instance->lowG) != 0) {
        coeffs_LOW = calcCoeffsLowShelf(*(instance->lowF),
                                        *(instance->lowG),
                                        *(instance->lowQ),
                                        instance->sampleRate);
        run2ndOrderBiquad(instance->output, sampleCount, coeffs_LOW, 1, instance->prevSamples_LOW);
    } else {
        instance->prevSamples_LOW->xprev1 = *(output - 1);
        instance->prevSamples_LOW->xprev2 = *(output - 2);
        instance->prevSamples_LOW->yprev1 = *(output - 1);
        instance->prevSamples_LOW->yprev2 = *(output - 2);
    }
    // FILTER PROCESSING FOR PEAKING EQ 1 ///////////////////////////////////////
    if (*(instance->p1G) != 0) {
        coeffs_P1 = calcCoeffsPeaking(*(instance->p1F),
                                      *(instance->p1G),
                                      *(instance->p1Q),
                                      instance->sampleRate);
        run2ndOrderBiquad(instance->output, sampleCount, coeffs_P1, 1, instance->prevSamples_P1);
    } else {
        instance->prevSamples_P1->xprev1 = *(output - 1);
        instance->prevSamples_P1->xprev2 = *(output - 2);
        instance->prevSamples_P1->yprev1 = *(output - 1);
        instance->prevSamples_P1->yprev2 = *(output - 2);
    }
    // FILTER PROCESSING FOR PEAKING EQ 2 ///////////////////////////////////////
    if (*(instance->p2G) != 0) {
        coeffs_P2 = calcCoeffsPeaking(*(instance->p2F),
                                      *(instance->p2G),
                                      *(instance->p2Q),
                                      instance->sampleRate);
        run2ndOrderBiquad(instance->output, sampleCount, coeffs_P2, 1, instance->prevSamples_P2);
    } else {
        instance->prevSamples_P2->xprev1 = *(output - 1);
        instance->prevSamples_P2->xprev2 = *(output - 2);
        instance->prevSamples_P2->yprev1 = *(output - 1);
        instance->prevSamples_P2->yprev2 = *(output - 2);
    }
    // FILTER PROCESSING FOR PEAKING EQ 3 ///////////////////////////////////////
    if (*(instance->p3G) != 0) {
        coeffs_P3 = calcCoeffsPeaking(*(instance->p3F),
                                      *(instance->p3G),
                                      *(instance->p3Q),
                                      instance->sampleRate);
        run2ndOrderBiquad(instance->output, sampleCount, coeffs_P3, 1, instance->prevSamples_P3);
    } else {
        instance->prevSamples_P3->xprev1 = *(output - 1);
        instance->prevSamples_P3->xprev2 = *(output - 2);
        instance->prevSamples_P3->yprev1 = *(output - 1);
        instance->prevSamples_P3->yprev2 = *(output - 2);
    }
    // FILTER PROCESSING FOR HIGH SHELF EQ //////////////////////////////////////
    if (*(instance->highG) != 0) {
        coeffs_HIGH = calcCoeffsHighShelf(*(instance->highF),
                                          *(instance->highG),
                                          *(instance->highQ),
                                          instance->sampleRate);
        run2ndOrderBiquad(instance->output, sampleCount, coeffs_HIGH, 1, instance->prevSamples_HIGH);
    } else {
        instance->prevSamples_HIGH->xprev1 = *(output - 1);
        instance->prevSamples_HIGH->xprev2 = *(output - 2);
        instance->prevSamples_HIGH->yprev1 = *(output - 1);
        instance->prevSamples_HIGH->yprev2 = *(output - 2);
    } 
}


/* Throw away a ThreeBandParametricEqWithShelves instance. */
void cleanupThreeBandParametricEqWithShelves(LADSPA_Handle Instance) {
    ThreeBandParametricEqWithShelves * instance;
    instance = (ThreeBandParametricEqWithShelves *)Instance;
    if (instance->mmapArea != NULL) {
        cleanupMmapFile("3BandParamEqWithShelves",
                        *(instance->mmapFname),
                        instance->created_s,
                        instance->created_ns);
        free(Instance);
    }
}


LADSPA_Descriptor * threeBandParametricEqWithShelvesInstanceDescriptor = NULL;


/* called automatically when the plugin library is first loaded. */
void plugin_init() {

    char** portNames;
    LADSPA_PortDescriptor* portDescriptors;
    LADSPA_PortRangeHint* portRangeHints;
    
    threeBandParametricEqWithShelvesInstanceDescriptor
      = (LADSPA_Descriptor *)malloc(sizeof(LADSPA_Descriptor));

    if (threeBandParametricEqWithShelvesInstanceDescriptor != NULL) {
    
        threeBandParametricEqWithShelvesInstanceDescriptor->UniqueID
            = 5541;
        threeBandParametricEqWithShelvesInstanceDescriptor->Label
            = strdup("3band_parameq_with_shelves");
        threeBandParametricEqWithShelvesInstanceDescriptor->Properties
            = LADSPA_PROPERTY_HARD_RT_CAPABLE;
        threeBandParametricEqWithShelvesInstanceDescriptor->Name 
            = strdup("T5's 3-Band Parametric with Shelves");
        threeBandParametricEqWithShelvesInstanceDescriptor->Maker
            = strdup("Juergen Herrmann (t-5@t-5.eu)");
        threeBandParametricEqWithShelvesInstanceDescriptor->Copyright
            = strdup("3-clause BSD licence");
        threeBandParametricEqWithShelvesInstanceDescriptor->PortCount
            = PORTCOUNT;
        portDescriptors
            = (LADSPA_PortDescriptor *)calloc(PORTCOUNT, sizeof(LADSPA_PortDescriptor));
        threeBandParametricEqWithShelvesInstanceDescriptor->PortDescriptors
            = (const LADSPA_PortDescriptor *)portDescriptors;
        portDescriptors[SF_INPUT]
            = LADSPA_PORT_INPUT | LADSPA_PORT_AUDIO;
        portDescriptors[SF_OUTPUT]
            = LADSPA_PORT_OUTPUT | LADSPA_PORT_AUDIO;
        portDescriptors[SF_LOW_F]
            = LADSPA_PORT_INPUT | LADSPA_PORT_CONTROL;
        portDescriptors[SF_LOW_G]
            = LADSPA_PORT_INPUT | LADSPA_PORT_CONTROL;
        portDescriptors[SF_LOW_Q]
            = LADSPA_PORT_INPUT | LADSPA_PORT_CONTROL;
        portDescriptors[SF_P1_F]
            = LADSPA_PORT_INPUT | LADSPA_PORT_CONTROL;
        portDescriptors[SF_P1_G]
            = LADSPA_PORT_INPUT | LADSPA_PORT_CONTROL;
        portDescriptors[SF_P1_Q]
            = LADSPA_PORT_INPUT | LADSPA_PORT_CONTROL;
        portDescriptors[SF_P2_F]
            = LADSPA_PORT_INPUT | LADSPA_PORT_CONTROL;
        portDescriptors[SF_P2_G]
            = LADSPA_PORT_INPUT | LADSPA_PORT_CONTROL;
        portDescriptors[SF_P2_Q]
            = LADSPA_PORT_INPUT | LADSPA_PORT_CONTROL;
        portDescriptors[SF_P3_F]
            = LADSPA_PORT_INPUT | LADSPA_PORT_CONTROL;
        portDescriptors[SF_P3_G]
            = LADSPA_PORT_INPUT | LADSPA_PORT_CONTROL;
        portDescriptors[SF_P3_Q]
            = LADSPA_PORT_INPUT | LADSPA_PORT_CONTROL;
        portDescriptors[SF_HIGH_F]
            = LADSPA_PORT_INPUT | LADSPA_PORT_CONTROL;
        portDescriptors[SF_HIGH_G]
            = LADSPA_PORT_INPUT | LADSPA_PORT_CONTROL;
        portDescriptors[SF_HIGH_Q]
            = LADSPA_PORT_INPUT | LADSPA_PORT_CONTROL;
        portDescriptors[SF_GAIN]
            = LADSPA_PORT_INPUT | LADSPA_PORT_CONTROL;
        portDescriptors[SF_MMAPFNAME]
            = LADSPA_PORT_INPUT | LADSPA_PORT_CONTROL;
        portNames
            = (char **)calloc(PORTCOUNT, sizeof(char *));
        threeBandParametricEqWithShelvesInstanceDescriptor->PortNames 
            = (const char **)portNames;
        portNames[SF_INPUT]
            = strdup("Input");
        portNames[SF_OUTPUT]
            = strdup("Output");
        portNames[SF_LOW_F]
            = strdup("Low Shelf Frequency [Hz]");
        portNames[SF_LOW_G]
            = strdup("Low Shelf Gain [dB]");
        portNames[SF_LOW_Q]
            = strdup("Low Shelf Q");
        portNames[SF_P1_F]
            = strdup("Peaking EQ 1 Frequency [Hz]");
        portNames[SF_P1_G]
            = strdup("Peaking EQ 1 Gain [dB]");
        portNames[SF_P1_Q]
            = strdup("Peaking EQ 1 Q");
        portNames[SF_P2_F]
            = strdup("Peaking EQ 2 Frequency [Hz]");
        portNames[SF_P2_G]
            = strdup("Peaking EQ 2 Gain [dB]");
        portNames[SF_P2_Q]
            = strdup("Peaking EQ 2 Q");
        portNames[SF_P3_F]
            = strdup("Peaking EQ 3 Frequency [Hz]");
        portNames[SF_P3_G]
            = strdup("Peaking EQ 3 Gain [dB]");
        portNames[SF_P3_Q]
            = strdup("Peaking EQ 3 Q");
        portNames[SF_HIGH_F]
            = strdup("High Shelf Frequency [Hz]");
        portNames[SF_HIGH_G]
            = strdup("High Shelf Gain [dB]");
        portNames[SF_HIGH_Q]
            = strdup("High Shelf Q");
        portNames[SF_GAIN]
            = strdup("Overall Gain [dB]");
        portNames[SF_MMAPFNAME]
            = strdup("MMAP-Filename-Part");
        portRangeHints = ((LADSPA_PortRangeHint *)
            calloc(PORTCOUNT, sizeof(LADSPA_PortRangeHint)));
        threeBandParametricEqWithShelvesInstanceDescriptor->PortRangeHints
            = (const LADSPA_PortRangeHint *)portRangeHints;
        // Low Shelf --------------------------------------------------------- */
        portRangeHints[SF_LOW_F].HintDescriptor
            = (LADSPA_HINT_BOUNDED_BELOW 
            | LADSPA_HINT_BOUNDED_ABOVE
            | LADSPA_HINT_SAMPLE_RATE
            | LADSPA_HINT_LOGARITHMIC
            | LADSPA_HINT_DEFAULT_440);
        portRangeHints[SF_LOW_F].LowerBound 
            = 0;
        portRangeHints[SF_LOW_F].UpperBound
            = 0.5;
        portRangeHints[SF_LOW_G].HintDescriptor
            = (LADSPA_HINT_BOUNDED_BELOW 
            | LADSPA_HINT_BOUNDED_ABOVE
            | LADSPA_HINT_DEFAULT_0);
        portRangeHints[SF_LOW_G].LowerBound 
            = -20;
        portRangeHints[SF_LOW_G].UpperBound
            = 20;
        portRangeHints[SF_LOW_Q].HintDescriptor
            = (LADSPA_HINT_BOUNDED_BELOW 
            | LADSPA_HINT_BOUNDED_ABOVE
            | LADSPA_HINT_DEFAULT_1);
        portRangeHints[SF_LOW_Q].LowerBound 
            = 0.1;
        portRangeHints[SF_LOW_Q].UpperBound
            = 10;
        // Peaking Parametric EQ 1 -------------------------------------------- */
        portRangeHints[SF_P1_F].HintDescriptor
            = (LADSPA_HINT_BOUNDED_BELOW 
            | LADSPA_HINT_BOUNDED_ABOVE
            | LADSPA_HINT_SAMPLE_RATE
            | LADSPA_HINT_LOGARITHMIC
            | LADSPA_HINT_DEFAULT_440);
        portRangeHints[SF_P1_F].LowerBound 
            = 0;
        portRangeHints[SF_P1_F].UpperBound
            = 0.5;
        portRangeHints[SF_P1_G].HintDescriptor
            = (LADSPA_HINT_BOUNDED_BELOW 
            | LADSPA_HINT_BOUNDED_ABOVE
            | LADSPA_HINT_DEFAULT_0);
        portRangeHints[SF_P1_G].LowerBound 
            = -20;
        portRangeHints[SF_P1_G].UpperBound
            = 20;
        portRangeHints[SF_P1_Q].HintDescriptor
            = (LADSPA_HINT_BOUNDED_BELOW 
            | LADSPA_HINT_BOUNDED_ABOVE
            | LADSPA_HINT_DEFAULT_1);
        portRangeHints[SF_P1_Q].LowerBound 
            = 0.1;
        portRangeHints[SF_P1_Q].UpperBound
            = 10;
        // Peaking Parametric EQ 2 -------------------------------------------- */
        portRangeHints[SF_P2_F].HintDescriptor
            = (LADSPA_HINT_BOUNDED_BELOW 
            | LADSPA_HINT_BOUNDED_ABOVE
            | LADSPA_HINT_SAMPLE_RATE
            | LADSPA_HINT_LOGARITHMIC
            | LADSPA_HINT_DEFAULT_440);
        portRangeHints[SF_P2_F].LowerBound 
            = 0;
        portRangeHints[SF_P2_F].UpperBound
            = 0.5;
        portRangeHints[SF_P2_G].HintDescriptor
            = (LADSPA_HINT_BOUNDED_BELOW 
            | LADSPA_HINT_BOUNDED_ABOVE
            | LADSPA_HINT_DEFAULT_0);
        portRangeHints[SF_P2_G].LowerBound 
            = -20;
        portRangeHints[SF_P2_G].UpperBound
            = 20;
        portRangeHints[SF_P2_Q].HintDescriptor
            = (LADSPA_HINT_BOUNDED_BELOW 
            | LADSPA_HINT_BOUNDED_ABOVE
            | LADSPA_HINT_DEFAULT_1);
        portRangeHints[SF_P2_Q].LowerBound 
            = 0.1;
        portRangeHints[SF_P2_Q].UpperBound
            = 10;
        // Peaking Parametric EQ 3 -------------------------------------------- */
        portRangeHints[SF_P3_F].HintDescriptor
            = (LADSPA_HINT_BOUNDED_BELOW 
            | LADSPA_HINT_BOUNDED_ABOVE
            | LADSPA_HINT_SAMPLE_RATE
            | LADSPA_HINT_LOGARITHMIC
            | LADSPA_HINT_DEFAULT_440);
        portRangeHints[SF_P3_F].LowerBound 
            = 0;
        portRangeHints[SF_P3_F].UpperBound
            = 0.5;
        portRangeHints[SF_P3_G].HintDescriptor
            = (LADSPA_HINT_BOUNDED_BELOW 
            | LADSPA_HINT_BOUNDED_ABOVE
            | LADSPA_HINT_DEFAULT_0);
        portRangeHints[SF_P3_G].LowerBound 
            = -20;
        portRangeHints[SF_P3_G].UpperBound
            = 20;
        portRangeHints[SF_P3_Q].HintDescriptor
            = (LADSPA_HINT_BOUNDED_BELOW 
            | LADSPA_HINT_BOUNDED_ABOVE
            | LADSPA_HINT_DEFAULT_1);
        portRangeHints[SF_P3_Q].LowerBound 
            = 0.1;
        portRangeHints[SF_P3_Q].UpperBound
            = 10;
        // High Shelf -------------------------------------------------------- */
        portRangeHints[SF_HIGH_F].HintDescriptor
            = (LADSPA_HINT_BOUNDED_BELOW 
            | LADSPA_HINT_BOUNDED_ABOVE
            | LADSPA_HINT_SAMPLE_RATE
            | LADSPA_HINT_LOGARITHMIC
            | LADSPA_HINT_DEFAULT_440);
        portRangeHints[SF_HIGH_F].LowerBound 
            = 0;
        portRangeHints[SF_HIGH_F].UpperBound
            = 0.5;
        portRangeHints[SF_HIGH_G].HintDescriptor
            = (LADSPA_HINT_BOUNDED_BELOW 
            | LADSPA_HINT_BOUNDED_ABOVE
            | LADSPA_HINT_DEFAULT_0);
        portRangeHints[SF_HIGH_G].LowerBound 
            = -20;
        portRangeHints[SF_HIGH_G].UpperBound
            = 20;
        portRangeHints[SF_HIGH_Q].HintDescriptor
            = (LADSPA_HINT_BOUNDED_BELOW 
            | LADSPA_HINT_BOUNDED_ABOVE
            | LADSPA_HINT_DEFAULT_1);
        portRangeHints[SF_HIGH_Q].LowerBound 
            = 0.1;
        portRangeHints[SF_HIGH_Q].UpperBound
            = 10;
        // Gain ------------------------------------------------------------ */
        portRangeHints[SF_GAIN].HintDescriptor
            = (LADSPA_HINT_BOUNDED_BELOW 
            | LADSPA_HINT_BOUNDED_ABOVE
            | LADSPA_HINT_DEFAULT_0);
        portRangeHints[SF_GAIN].LowerBound 
            = -20;
        portRangeHints[SF_GAIN].UpperBound
            = 20;
        // MMAP Filename --------------------------------------------------- */
        portRangeHints[SF_MMAPFNAME].HintDescriptor
            = (LADSPA_HINT_BOUNDED_BELOW 
            | LADSPA_HINT_BOUNDED_ABOVE
            | LADSPA_HINT_DEFAULT_0);
        portRangeHints[SF_MMAPFNAME].LowerBound 
            = 0;
        portRangeHints[SF_MMAPFNAME].UpperBound
            = 10000000000;
        // In- and Outpus -------------------------------------------------- */
        portRangeHints[SF_INPUT].HintDescriptor
            = 0;
        portRangeHints[SF_OUTPUT].HintDescriptor
            = 0;
        threeBandParametricEqWithShelvesInstanceDescriptor->instantiate 
            = instantiateThreeBandParametricEqWithShelves;
        threeBandParametricEqWithShelvesInstanceDescriptor->connect_port 
            = connectPortToThreeBandParametricEqWithShelves;
        threeBandParametricEqWithShelvesInstanceDescriptor->activate
            = activateThreeBandParametricEqWithShelves;
        threeBandParametricEqWithShelvesInstanceDescriptor->run
            = runThreeBandParametricEqWithShelves;
        threeBandParametricEqWithShelvesInstanceDescriptor->run_adding
            = NULL;
        threeBandParametricEqWithShelvesInstanceDescriptor->set_run_adding_gain
            = NULL;
        threeBandParametricEqWithShelvesInstanceDescriptor->deactivate
            = NULL;
        threeBandParametricEqWithShelvesInstanceDescriptor->cleanup
            = cleanupThreeBandParametricEqWithShelves;
    }
}
  

/* called automatically when the library is unloaded. */
void plugin_deinit() {
    deleteDescriptor(threeBandParametricEqWithShelvesInstanceDescriptor);
}


/* Return a descriptor of the requested plugin types. */
const LADSPA_Descriptor * ladspa_descriptor(unsigned long index) {
    /* Return the requested descriptor or null if the index is out of range. */
    switch (index) {
    case 0:
        return threeBandParametricEqWithShelvesInstanceDescriptor;
    default:
        return NULL;
    }
}
