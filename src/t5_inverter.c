/* t5_inverter.c

   Free software by Juergen Herrmann, t-5@t-5.eu. Do with it, whatever you
   want. No warranty. None, whatsoever. Also see license.txt .

   This LADSPA plugin provides a sample accurate delay with up to 256k
   samples delay (maximum actual wallclock time delay available depends on
   the sample rate used)

   This file has poor memory protection. Failures during malloc() will
   not recover nicely. */

/*****************************************************************************/

#include <math.h>
#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <string.h>
#include <sys/mman.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <time.h>
#include <ladspa.h>
#include "helpers.h"


#define PORT_INPUT       0
#define PORT_OUTPUT      1
#define PORT_GAIN        2
#define PORT_MMAPFNAME   3
#define PORTCOUNT        4


static void plugin_init(void) __attribute__((constructor)); 
static void plugin_deinit(void) __attribute__((destructor)); 


/* instance data for the Inverter filter */
typedef struct {
    long created_ns;
    time_t created_s;
    LADSPA_Data sampleRate;
    LADSPA_Data* mmapArea;
    LADSPA_Data* mmapFname;
    LADSPA_Data* gain;
    // port pointers
    LADSPA_Data* input;
    LADSPA_Data* output;
} Inverter;


/* setup shared memory area to enable parametrization at runtime from external processes */
void setupMmapFileForInverter(Inverter* instance) {
    TimeMmapStruct ret;
    ret = setupMmapFile("Inverter", *(instance->mmapFname), PORTCOUNT); 
    instance->mmapArea = ret.mmap;
    instance->created_s = ret.s;
    instance->created_ns = ret.ns;
}


/* Construct a new plugin instance. */
LADSPA_Handle instantiateInverter(const LADSPA_Descriptor* descriptor,
                                     unsigned long sampleRate) {
    Inverter* instance;
    instance = (Inverter*)malloc(sizeof(Inverter));
    if (instance) {
        instance->sampleRate = (LADSPA_Data)sampleRate;
        instance->mmapArea = NULL;
    }  
    return instance;
}


/* Initialise and activate a plugin instance. */
void activateInverter(LADSPA_Handle ladspa_handle) {
    Inverter* instance;
    instance = (Inverter*)ladspa_handle;
    *(instance->gain) = 0;
}


/* Connect a port to a data location.  */
void connectPortToInverter(LADSPA_Handle ladspa_handle,
                              unsigned long port,
                              LADSPA_Data* dataLocation) {  
    Inverter* instance;
    instance = (Inverter*)ladspa_handle;
    switch (port) {
    case PORT_INPUT:
        instance->input = dataLocation;
        break;
    case PORT_OUTPUT:
        instance->output = dataLocation;
        break;
    case PORT_GAIN:
        instance->gain = dataLocation;
        break;
    case PORT_MMAPFNAME:
        instance->mmapFname = dataLocation;
        break;
    }
}


/* Run the filter algorithm for a block of SampleCount samples. */
void runInverter(LADSPA_Handle ladspa_handle,
                    unsigned long SampleCount) {

    LADSPA_Data* input;
    LADSPA_Data* output;
    Inverter* instance;
    unsigned long sampleIndex;
    LADSPA_Data unchanged = 0.0;
    LADSPA_Data changed;
    LADSPA_Data gainFactor;
    LADSPA_Data* mmptr;

    instance = (Inverter*)ladspa_handle;
    input = instance->input;
    output = instance->output;
    // memcpy parameters over from mmapped area
    mmptr = instance->mmapArea;
    if (mmptr != NULL) {
        memcpy(&changed, mmptr, sizeof(LADSPA_Data));
        if (changed != 0.0) {
            mmptr += 1;
            memcpy(instance->gain, mmptr, sizeof(LADSPA_Data));
        }
        // reset changed flag to re-enable parametrization by control inputs
        memcpy(instance->mmapArea, &unchanged, sizeof(LADSPA_Data));
    } else if (*(instance->mmapFname) != 0.0) {
        setupMmapFileForInverter(instance);
    }
    gainFactor = dbToGainFactor(*(instance->gain)) * -1.0;
    for (sampleIndex = 0; sampleIndex < SampleCount; sampleIndex++) {
        *(output++) = *(input++) * gainFactor;
    }
}


/* Throw away a Inverter instance. */
void cleanupInverter(LADSPA_Handle ladspa_handle) {
    Inverter * instance;
    instance = (Inverter*)ladspa_handle;
    if (instance->mmapArea != NULL) {
        cleanupMmapFile("Inverter",
                        *(instance->mmapFname),
                        instance->created_s,
                        instance->created_ns);
        free(instance);
    }
}


LADSPA_Descriptor * inverterInstanceDescriptor = NULL;


/* called automatically when the plugin library is first loaded. */
void plugin_init() {

    char ** pcportNames;
    LADSPA_PortDescriptor * portDescriptors;
    LADSPA_PortRangeHint * portRangeHints;
    
    inverterInstanceDescriptor
      = (LADSPA_Descriptor *)malloc(sizeof(LADSPA_Descriptor));

    if (inverterInstanceDescriptor != NULL) {
    
        inverterInstanceDescriptor->UniqueID
            = 5556;
        inverterInstanceDescriptor->Label
            = strdup("inverter");
        inverterInstanceDescriptor->Properties
            = LADSPA_PROPERTY_HARD_RT_CAPABLE;
        inverterInstanceDescriptor->Name 
            = strdup("T5's Inverter");
        inverterInstanceDescriptor->Maker
            = strdup("Juergen Herrmann (t-5@t-5.eu)");
        inverterInstanceDescriptor->Copyright
            = strdup("3-clause BSD licence");
        inverterInstanceDescriptor->PortCount
            = PORTCOUNT;
        portDescriptors
            = (LADSPA_PortDescriptor*)calloc(PORTCOUNT, sizeof(LADSPA_PortDescriptor));
        inverterInstanceDescriptor->PortDescriptors
            = (const LADSPA_PortDescriptor*)portDescriptors;
        portDescriptors[PORT_INPUT]
            = LADSPA_PORT_INPUT | LADSPA_PORT_AUDIO;
        portDescriptors[PORT_OUTPUT]
            = LADSPA_PORT_OUTPUT | LADSPA_PORT_AUDIO;
        portDescriptors[PORT_GAIN]
            = LADSPA_PORT_INPUT | LADSPA_PORT_CONTROL;
        portDescriptors[PORT_MMAPFNAME]
            = LADSPA_PORT_INPUT | LADSPA_PORT_CONTROL;
        pcportNames
            = (char **)calloc(PORTCOUNT, sizeof(char *));
        inverterInstanceDescriptor->PortNames 
            = (const char **)pcportNames;
        pcportNames[PORT_INPUT]
            = strdup("Input");
        pcportNames[PORT_OUTPUT]
            = strdup("Output");
        pcportNames[PORT_GAIN]
            = strdup("Gain [dB]");
        pcportNames[PORT_MMAPFNAME]
            = strdup("MMAP-Filename-Part");
        portRangeHints = ((LADSPA_PortRangeHint*)
            calloc(PORTCOUNT, sizeof(LADSPA_PortRangeHint)));
        inverterInstanceDescriptor->PortRangeHints
            = (const LADSPA_PortRangeHint*)portRangeHints;
        // Gain ------------------------------------------------------------ */
        portRangeHints[PORT_GAIN].HintDescriptor
            = (LADSPA_HINT_BOUNDED_BELOW 
            | LADSPA_HINT_BOUNDED_ABOVE
            | LADSPA_HINT_DEFAULT_0);
        portRangeHints[PORT_GAIN].LowerBound 
            = -12;
        portRangeHints[PORT_GAIN].UpperBound
            = 12;
        // MMAP Filename --------------------------------------------------- */
        portRangeHints[PORT_MMAPFNAME].HintDescriptor
            = (LADSPA_HINT_BOUNDED_BELOW 
            | LADSPA_HINT_BOUNDED_ABOVE
            | LADSPA_HINT_DEFAULT_0);
        portRangeHints[PORT_MMAPFNAME].LowerBound 
            = 0;
        portRangeHints[PORT_MMAPFNAME].UpperBound
            = 10000000000;
        // In- and Outpus -------------------------------------------------- */
        portRangeHints[PORT_INPUT].HintDescriptor
            = 0;
        portRangeHints[PORT_OUTPUT].HintDescriptor
            = 0;
        inverterInstanceDescriptor->instantiate 
            = instantiateInverter;
        inverterInstanceDescriptor->connect_port 
            = connectPortToInverter;
        inverterInstanceDescriptor->activate
            = activateInverter;
        inverterInstanceDescriptor->run
            = runInverter;
        inverterInstanceDescriptor->run_adding
            = NULL;
        inverterInstanceDescriptor->set_run_adding_gain
            = NULL;
        inverterInstanceDescriptor->deactivate
            = NULL;
        inverterInstanceDescriptor->cleanup
            = cleanupInverter;
    }
}
  

/* called automatically when the library is unloaded. */
void plugin_deinit() {
    deleteDescriptor(inverterInstanceDescriptor);
}


/* Return a descriptor of the requested plugin types. */
const LADSPA_Descriptor * ladspa_descriptor(unsigned long Index) {
    /* Return the requested descriptor or null if the index is out of range. */
    switch (Index) {
    case 0:
        return inverterInstanceDescriptor;
    default:
        return NULL;
    }
}
