/* t5_linkwitz_transform.c

   Free software by Juergen Herrmann, t-5@t-5.eu. Do with it, whatever you
   want. No warranty. None, whatsoever. Also see license.txt .

   This LADSPA plugin provides a Linkwitz Transform filter.

   This file has poor memory protection. Failures during malloc() will
   not recover nicely. */

/*****************************************************************************/

#include <math.h>
#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <string.h>
#include <sys/mman.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <time.h>
#include <ladspa.h>
#include "helpers.h"
#include "runbiquad.h"


#define SF_INPUT       0
#define SF_OUTPUT      1
#define SF_F           2
#define SF_Q           3
#define SF_F0          4
#define SF_Q0          5
#define SF_GAIN        6
#define SF_MMAPFNAME   7
#define PORTCOUNT      8


static void plugin_init(void) __attribute__((constructor)); 
static void plugin_deinit(void) __attribute__((destructor)); 


/* Instance data for the Linkwitz Transform Filter */
typedef struct {
    long created_ns;
    time_t created_s;
    LADSPA_Data sampleRate;
    LADSPA_Data* mmapArea;
    LADSPA_Data* mmapFname;
    // port pointers
    LADSPA_Data* input;
    LADSPA_Data* output;
    LADSPA_Data* f;
    LADSPA_Data* q;
    LADSPA_Data* f0;
    LADSPA_Data* q0;
    LADSPA_Data* gain;
    // storage for previously processed samples
    RunBiquadPrevSamples* prevSamples;
} LinkwitzTransform;


/* Construct a new LinkwitzTransform plugin instance. */
LADSPA_Handle instantiateLinkwitzTransform(const LADSPA_Descriptor* descriptor,
                                           unsigned long sampleRate) {
    LinkwitzTransform* instance;
    instance = (LinkwitzTransform*)malloc(sizeof(LinkwitzTransform));
    if (instance) {
        instance->sampleRate = (LADSPA_Data)sampleRate;
        instance->mmapArea = NULL;
        instance->prevSamples = malloc(sizeof(RunBiquadPrevSamples));
    }  
    return instance;
}


/* Initialise and activate a SecondOrder(Low|High)Pass plugin instance. */
void activateLinkwitzTransform(LADSPA_Handle ladspaHandle);
void activateLinkwitzTransform(LADSPA_Handle ladspaHandle) {
    LinkwitzTransform* instance;
    instance = (LinkwitzTransform*)ladspaHandle;
    instance->prevSamples->xprev1 = 0;
    instance->prevSamples->yprev1 = 0;
    instance->prevSamples->xprev2 = 0;
    instance->prevSamples->yprev2 = 0;
}


/* Connect a SecondOrder(Low|High)Pass port to a data location.  */
void connectPortToLinkwitzTransform(LADSPA_Handle ladspaHandle,
                                    unsigned long port,
                                    LADSPA_Data* dataLocation);
void connectPortToLinkwitzTransform(LADSPA_Handle ladspaHandle,
                                    unsigned long port,
                                    LADSPA_Data* dataLocation) {
  
    LinkwitzTransform* instance;
    instance = (LinkwitzTransform*)ladspaHandle;

    switch (port) {
    case SF_INPUT:
        instance->input = dataLocation;
        break;
    case SF_OUTPUT:
        instance->output = dataLocation;
        break;
    case SF_F:
        instance->f = dataLocation;
        break;
    case SF_Q:
        instance->q = dataLocation;
        break;
    case SF_F0:
        instance->f0 = dataLocation;
        break;
    case SF_Q0:
        instance->q0 = dataLocation;
        break;
    case SF_GAIN:
        instance->gain = dataLocation;
        break;
    case SF_MMAPFNAME:
        instance->mmapFname = dataLocation;
        break;
    }
}


void runLinkwitzTransformBiquad(LADSPA_Handle ladspaHandle, unsigned long sampleCount, BiquadCoeffs coeffs);
void runLinkwitzTransformBiquad(LADSPA_Handle ladspaHandle, unsigned long sampleCount, BiquadCoeffs coeffs) {
    LinkwitzTransform* instance;
    LADSPA_Data* input;
    LADSPA_Data* output;
    LADSPA_Data unchanged = 0.0;
    LADSPA_Data changed;
    LADSPA_Data* mmptr;
    unsigned long sampleIndex;
    LADSPA_Data gainFactor;

    instance = (LinkwitzTransform*)ladspaHandle;
    input = instance->input;
    output = instance->output;
    // memcpy parameters over from mmapped area
    mmptr = instance->mmapArea;
    if (mmptr != NULL) {
        memcpy(&changed, mmptr, sizeof(LADSPA_Data));
        if (changed != 0.0) {
            mmptr += 1;
            memcpy(instance->f, mmptr, sizeof(LADSPA_Data));
            mmptr += 1;
            memcpy(instance->q, mmptr, sizeof(LADSPA_Data));
            mmptr += 1;
            memcpy(instance->f0, mmptr, sizeof(LADSPA_Data));
            mmptr += 1;
            memcpy(instance->q0, mmptr, sizeof(LADSPA_Data));
            mmptr += 1;
            memcpy(instance->gain, mmptr, sizeof(LADSPA_Data));
        }
        // reset changed flag to re-enable parametrization by control inputs
        memcpy(instance->mmapArea, &unchanged, sizeof(LADSPA_Data));
    }
    // apply gain if gainFactor != 1, in any case make sure output buf is filled
    // as it will be used by the run2ndOrderBiquad() function.
    gainFactor = dbToGainFactor(*(instance->gain));
    if (gainFactor == 1.0) {
        for (sampleIndex = 0; sampleIndex < sampleCount; sampleIndex++) {
            *(output++) = *(input++);
        }
    } else {
        for (sampleIndex = 0; sampleIndex < sampleCount; sampleIndex++) {
            *(output++) = *(input++) * gainFactor;
        }
    }
    run2ndOrderBiquad(instance->output, sampleCount, coeffs, 1, instance->prevSamples);
}


/* setup shared memory area to enable parametrization at runtime from external processes */
void setupMmapFileForLinkwitzTransform(LinkwitzTransform* instance) {
    TimeMmapStruct ret;
    ret = setupMmapFile("LinkwitzTransform", *(instance->mmapFname), PORTCOUNT); 
    instance->mmapArea = ret.mmap;
    instance->created_s = ret.s;
    instance->created_ns = ret.ns;
}


/* Run the filter algorithm for a block of SampleCount samples. */
void runLinkwitzTransform(LADSPA_Handle ladspaHandle, unsigned long sampleCount) {
    BiquadCoeffs coeffs;
    LinkwitzTransform* instance;
    instance = (LinkwitzTransform*)ladspaHandle;
    if (instance->mmapArea == NULL && *(instance->mmapFname) != 0.0) {
        setupMmapFileForLinkwitzTransform(instance);
    }
    coeffs = calcCoeffsLinkwitzTransform(*(instance->f), *(instance->q),
                                         *(instance->f0), *(instance->q0),
                                         instance->sampleRate);
    runLinkwitzTransformBiquad(instance, sampleCount, coeffs);
}


/* Throw away a SoHighPass instance. */
void cleanupLinkwitzTransform(LADSPA_Handle ladspaHandle) {
    LinkwitzTransform* instance;
    instance = (LinkwitzTransform*)ladspaHandle;
    if (instance->mmapArea != NULL) {
        cleanupMmapFile("LinkwitzTransform",
                        *(instance->mmapFname),
                        instance->created_s,
                        instance->created_ns);
        free(ladspaHandle);
    }
}


LADSPA_Descriptor * linkwitzTransformInstanceDescriptor = NULL;


/* called automatically when the plugin library is Second loaded. */
void plugin_init() {

    char** portNames;
    LADSPA_PortDescriptor* portDescriptors;
    LADSPA_PortRangeHint* portRangeHints;
    
    linkwitzTransformInstanceDescriptor
      = (LADSPA_Descriptor *)malloc(sizeof(LADSPA_Descriptor));

    if (linkwitzTransformInstanceDescriptor != NULL) {
    
        linkwitzTransformInstanceDescriptor->UniqueID
            = 5557;
        linkwitzTransformInstanceDescriptor->Label
            = strdup("linkwitz_transform");
        linkwitzTransformInstanceDescriptor->Properties
            = LADSPA_PROPERTY_HARD_RT_CAPABLE;
        linkwitzTransformInstanceDescriptor->Name 
            = strdup("T5's Linkwitz Transform Filter");
        linkwitzTransformInstanceDescriptor->Maker
            = strdup("Juergen Herrmann (t-5@t-5.eu)");
        linkwitzTransformInstanceDescriptor->Copyright
            = strdup("3-clause BSD licence");
        linkwitzTransformInstanceDescriptor->PortCount
            = PORTCOUNT;
        portDescriptors
            = (LADSPA_PortDescriptor *)calloc(PORTCOUNT, sizeof(LADSPA_PortDescriptor));
        linkwitzTransformInstanceDescriptor->PortDescriptors
            = (const LADSPA_PortDescriptor *)portDescriptors;
        portDescriptors[SF_INPUT]
            = LADSPA_PORT_INPUT | LADSPA_PORT_AUDIO;
        portDescriptors[SF_OUTPUT]
            = LADSPA_PORT_OUTPUT | LADSPA_PORT_AUDIO;
        portDescriptors[SF_F]
            = LADSPA_PORT_INPUT | LADSPA_PORT_CONTROL;
        portDescriptors[SF_Q]
            = LADSPA_PORT_INPUT | LADSPA_PORT_CONTROL;
        portDescriptors[SF_F0]
            = LADSPA_PORT_INPUT | LADSPA_PORT_CONTROL;
        portDescriptors[SF_Q0]
            = LADSPA_PORT_INPUT | LADSPA_PORT_CONTROL;
        portDescriptors[SF_GAIN]
            = LADSPA_PORT_INPUT | LADSPA_PORT_CONTROL;
        portDescriptors[SF_MMAPFNAME]
            = LADSPA_PORT_INPUT | LADSPA_PORT_CONTROL;
        portNames
            = (char **)calloc(PORTCOUNT, sizeof(char *));
        linkwitzTransformInstanceDescriptor->PortNames 
            = (const char **)portNames;
        portNames[SF_INPUT]
            = strdup("Input");
        portNames[SF_OUTPUT]
            = strdup("Output");
        portNames[SF_F]
            = strdup("Target Frequency [Hz]");
        portNames[SF_Q]
            = strdup("Target Q");
        portNames[SF_F0]
            = strdup("Original Frequency [Hz]");
        portNames[SF_Q0]
            = strdup("Original Q");
        portNames[SF_GAIN]
            = strdup("Overall Gain [dB]");
        portNames[SF_MMAPFNAME]
            = strdup("MMAP-Filename-Part");
        portRangeHints = ((LADSPA_PortRangeHint *)
            calloc(PORTCOUNT, sizeof(LADSPA_PortRangeHint)));
        linkwitzTransformInstanceDescriptor->PortRangeHints
            = (const LADSPA_PortRangeHint *)portRangeHints;
        // F --------------------------------------------------------------- */
        portRangeHints[SF_F].HintDescriptor
            = (LADSPA_HINT_BOUNDED_BELOW 
            | LADSPA_HINT_BOUNDED_ABOVE
            | LADSPA_HINT_SAMPLE_RATE
            | LADSPA_HINT_LOGARITHMIC
            | LADSPA_HINT_DEFAULT_440);
        portRangeHints[SF_F].LowerBound 
            = 0;
        portRangeHints[SF_F].UpperBound
            = 0.5;
        // Q --------------------------------------------------------------- */
        portRangeHints[SF_Q].HintDescriptor
            = (LADSPA_HINT_BOUNDED_BELOW 
            | LADSPA_HINT_BOUNDED_ABOVE
            | LADSPA_HINT_LOGARITHMIC
            | LADSPA_HINT_DEFAULT_1);
        portRangeHints[SF_Q].LowerBound 
            = 0.1;
        portRangeHints[SF_Q].UpperBound
            = 10;
        // F0 -------------------------------------------------------------- */
        portRangeHints[SF_F0].HintDescriptor
            = (LADSPA_HINT_BOUNDED_BELOW 
            | LADSPA_HINT_BOUNDED_ABOVE
            | LADSPA_HINT_SAMPLE_RATE
            | LADSPA_HINT_LOGARITHMIC
            | LADSPA_HINT_DEFAULT_440);
        portRangeHints[SF_F0].LowerBound 
            = 0;
        portRangeHints[SF_F0].UpperBound
            = 0.5;
        // Q0 -------------------------------------------------------------- */
        portRangeHints[SF_Q0].HintDescriptor
            = (LADSPA_HINT_BOUNDED_BELOW 
            | LADSPA_HINT_BOUNDED_ABOVE
            | LADSPA_HINT_LOGARITHMIC
            | LADSPA_HINT_DEFAULT_1);
        portRangeHints[SF_Q0].LowerBound 
            = 0.1;
        portRangeHints[SF_Q0].UpperBound
            = 10;
        // Gain ------------------------------------------------------------ */
        portRangeHints[SF_GAIN].HintDescriptor
            = (LADSPA_HINT_BOUNDED_BELOW 
            | LADSPA_HINT_BOUNDED_ABOVE
            | LADSPA_HINT_DEFAULT_0);
        portRangeHints[SF_GAIN].LowerBound 
            = -20;
        portRangeHints[SF_GAIN].UpperBound
            = 20;
        // MMAP Filename --------------------------------------------------- */
        portRangeHints[SF_MMAPFNAME].HintDescriptor
            = (LADSPA_HINT_BOUNDED_BELOW 
            | LADSPA_HINT_BOUNDED_ABOVE
            | LADSPA_HINT_DEFAULT_0);
        portRangeHints[SF_MMAPFNAME].LowerBound 
            = 0;
        portRangeHints[SF_MMAPFNAME].UpperBound
            = 10000000000;
        // In- and Outpus -------------------------------------------------- */
        portRangeHints[SF_INPUT].HintDescriptor
            = 0;
        portRangeHints[SF_OUTPUT].HintDescriptor
            = 0;
        linkwitzTransformInstanceDescriptor->instantiate 
            = instantiateLinkwitzTransform;
        linkwitzTransformInstanceDescriptor->connect_port 
            = connectPortToLinkwitzTransform;
        linkwitzTransformInstanceDescriptor->activate
            = activateLinkwitzTransform;
        linkwitzTransformInstanceDescriptor->run
            = runLinkwitzTransform;
        linkwitzTransformInstanceDescriptor->run_adding
            = NULL;
        linkwitzTransformInstanceDescriptor->set_run_adding_gain
            = NULL;
        linkwitzTransformInstanceDescriptor->deactivate
            = NULL;
        linkwitzTransformInstanceDescriptor->cleanup
            = cleanupLinkwitzTransform;
    }
}
  

/* called automatically when the library is unloaded. */
void plugin_deinit() {
    deleteDescriptor(linkwitzTransformInstanceDescriptor);
}


/* Return a descriptor of the requested plugin types. */
const LADSPA_Descriptor * ladspa_descriptor(unsigned long index) {
    /* Return the requested descriptor or null if the index is out of range. */
    switch (index) {
    case 0:
        return linkwitzTransformInstanceDescriptor;
    default:
        return NULL;
    }
}
