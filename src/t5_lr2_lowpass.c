/* t5_lr2_lowpass.c

   Free software by Juergen Herrmann, t-5@t-5.eu. Do with it, whatever you
   want. No warranty. None, whatsoever. Also see license.txt .

   This LADSPA plugin provides a Linkwitz-Riley 12dB/octave low pass filter.

   This file has poor memory protection. Failures during malloc() will
   not recover nicely. */

/*****************************************************************************/

#include <math.h>
#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <string.h>
#include <sys/mman.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <time.h>
#include <ladspa.h>
#include "helpers.h"
#include "lr24.h"


static void plugin_init(void) __attribute__((constructor)); 
static void plugin_deinit(void) __attribute__((destructor)); 


/* setup shared memory area to enable parametrization at runtime from external processes */
void setupMmapFileForLr2Lowpass(Lr24LowHighPass* instance) {
    TimeMmapStruct ret;
    ret = setupMmapFile("Lr2Lowpass", *(instance->mmapFname), PORTCOUNT); 
    instance->mmapArea = ret.mmap;
    instance->created_s = ret.s;
    instance->created_ns = ret.ns;
}


/* Run the filter algorithm for a block of SampleCount samples. */
void runLr2Lowpass(LADSPA_Handle ladspaHandle, unsigned long sampleCount) {
    BiquadCoeffs coeffs;
    Lr24LowHighPass* instance;
    instance = (Lr24LowHighPass*)ladspaHandle;
    if (instance->mmapArea == NULL && *(instance->mmapFname) != 0.0) {
        setupMmapFileForLr2Lowpass(instance);
    }
    coeffs = calcCoeffs1stOrderLowpass(*(instance->f), instance->sampleRate);
    runLr2Biquad(instance, sampleCount, coeffs);
} 


/* Throw away a Lr2LowPass instance. */
void cleanupLr2Lowpass(LADSPA_Handle ladspaHandle) {
    Lr24LowHighPass* instance;
    instance = (Lr24LowHighPass*)ladspaHandle;
    if (instance->mmapArea != NULL) {
        cleanupMmapFile("Lr2Lowpass",
                        *(instance->mmapFname),
                        instance->created_s,
                        instance->created_ns);
        free(ladspaHandle);
    }
}


LADSPA_Descriptor* lr2LowpassInstanceDescriptor = NULL;


/* called automatically when the plugin library is first loaded. */
void plugin_init() {

    char** portNames;
    LADSPA_PortDescriptor* portDescriptors;
    LADSPA_PortRangeHint* portRangeHints;
    
    lr2LowpassInstanceDescriptor
      = (LADSPA_Descriptor*)malloc(sizeof(LADSPA_Descriptor));

    if (lr2LowpassInstanceDescriptor != NULL) {
    
        lr2LowpassInstanceDescriptor->UniqueID
            = 5544;
        lr2LowpassInstanceDescriptor->Label
            = strdup("lr2_lowpass");
        lr2LowpassInstanceDescriptor->Properties
            = LADSPA_PROPERTY_HARD_RT_CAPABLE;
        lr2LowpassInstanceDescriptor->Name 
            = strdup("T5's LR-2 Low Pass");
        lr2LowpassInstanceDescriptor->Maker
            = strdup("Juergen Herrmann (t-5@t-5.eu)");
        lr2LowpassInstanceDescriptor->Copyright
            = strdup("3-clause BSD licence");
        lr2LowpassInstanceDescriptor->PortCount
            = PORTCOUNT;
        portDescriptors
            = (LADSPA_PortDescriptor *)calloc(PORTCOUNT, sizeof(LADSPA_PortDescriptor));
        lr2LowpassInstanceDescriptor->PortDescriptors
            = (const LADSPA_PortDescriptor*)portDescriptors;
        portDescriptors[SF_INPUT]
            = LADSPA_PORT_INPUT | LADSPA_PORT_AUDIO;
        portDescriptors[SF_OUTPUT]
            = LADSPA_PORT_OUTPUT | LADSPA_PORT_AUDIO;
        portDescriptors[SF_F]
            = LADSPA_PORT_INPUT | LADSPA_PORT_CONTROL;
        portDescriptors[SF_GAIN]
            = LADSPA_PORT_INPUT | LADSPA_PORT_CONTROL;
        portDescriptors[SF_MMAPFNAME]
            = LADSPA_PORT_INPUT | LADSPA_PORT_CONTROL;
        portNames
            = (char **)calloc(PORTCOUNT, sizeof(char *));
        lr2LowpassInstanceDescriptor->PortNames 
            = (const char **)portNames;
        portNames[SF_INPUT]
            = strdup("Input");
        portNames[SF_OUTPUT]
            = strdup("Output");
        portNames[SF_F]
            = strdup("Cutoff Frequency [Hz]");
        portNames[SF_GAIN]
            = strdup("Overall Gain [dB]");
        portNames[SF_MMAPFNAME]
            = strdup("MMAP-Filename-Part");
        portRangeHints = ((LADSPA_PortRangeHint *)
            calloc(PORTCOUNT, sizeof(LADSPA_PortRangeHint)));
        lr2LowpassInstanceDescriptor->PortRangeHints
            = (const LADSPA_PortRangeHint *)portRangeHints;
        portRangeHints[SF_F].HintDescriptor
            = (LADSPA_HINT_BOUNDED_BELOW 
            | LADSPA_HINT_BOUNDED_ABOVE
            | LADSPA_HINT_SAMPLE_RATE
            | LADSPA_HINT_LOGARITHMIC
            | LADSPA_HINT_DEFAULT_440);
        portRangeHints[SF_F].LowerBound 
            = 0;
        portRangeHints[SF_F].UpperBound
            = 0.5;
        // Gain ------------------------------------------------------------ */
        portRangeHints[SF_GAIN].HintDescriptor
            = (LADSPA_HINT_BOUNDED_BELOW 
            | LADSPA_HINT_BOUNDED_ABOVE
            | LADSPA_HINT_DEFAULT_0);
        portRangeHints[SF_GAIN].LowerBound 
            = -20;
        portRangeHints[SF_GAIN].UpperBound
            = 20;
        // MMAP Filename --------------------------------------------------- */
        portRangeHints[SF_MMAPFNAME].HintDescriptor
            = (LADSPA_HINT_BOUNDED_BELOW 
            | LADSPA_HINT_BOUNDED_ABOVE
            | LADSPA_HINT_DEFAULT_0);
        portRangeHints[SF_MMAPFNAME].LowerBound 
            = 0;
        portRangeHints[SF_MMAPFNAME].UpperBound
            = 10000000000;
        // In- and Outpus -------------------------------------------------- */
        portRangeHints[SF_INPUT].HintDescriptor
            = 0;
        portRangeHints[SF_OUTPUT].HintDescriptor
            = 0;
        lr2LowpassInstanceDescriptor->instantiate 
            = instantiateLr24LowHighPass;
        lr2LowpassInstanceDescriptor->connect_port 
            = connectPortToLr24LowHighPass;
        lr2LowpassInstanceDescriptor->activate
            = activateLr24LowHighPass;
        lr2LowpassInstanceDescriptor->run
            = runLr2Lowpass;
        lr2LowpassInstanceDescriptor->run_adding
            = NULL;
        lr2LowpassInstanceDescriptor->set_run_adding_gain
            = NULL;
        lr2LowpassInstanceDescriptor->deactivate
            = NULL;
        lr2LowpassInstanceDescriptor->cleanup
            = cleanupLr2Lowpass;
    }
}
  

/* called automatically when the library is unloaded. */
void plugin_deinit() {
    deleteDescriptor(lr2LowpassInstanceDescriptor);
}


/* Return a descriptor of the requested plugin types. */
const LADSPA_Descriptor * ladspa_descriptor(unsigned long index) {
    /* Return the requested descriptor or null if the index is out of range. */
    switch (index) {
    case 0:
        return lr2LowpassInstanceDescriptor;
    default:
        return NULL;
    }
}
