ToDos
=====

    [x] subsample delay
    [x] 6dB/oct low/high pass filters
    [x] LR-2 12dB/oct low/high pass filters
    [x] improvement: only cycle samples in apply functions if gains != 0.0
    [x] implemented lr4_lowpass and lr4_highpass
    [x] factored out helper functions into helpers.h
    [x] add an addition parameter MMAPFNAME to control inputs which is used in
        the filename of mmap files in /dev/shm (to help identify different instances
        of the plugins wehn used in PaXoverRack)
    [x] rename .so name to t5_3band_parameq_with_shelves
    [x] get proper ladspa ids
    [x] rename label to 3band_parameq_with_shelves
    [x] rename struct and functions from BiquadEq to ThreeBandParaqmEqWithSelhves
